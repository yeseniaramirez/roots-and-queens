class Solution:
    def solveNQueens(self, n: int) -> list[list[str]]:
        ans = [[]]

        def QueensVision(y, x, g):
            ## Invalidate positions within the queens view. 
            ## y, x are the position of the queen. 
            for dir in [(0,1),(1,0),(-1,0),(0,-1),(1,1),(1,-1),(-1,1),(-1,-1)]:
                qY, qX = y+dir[0], x+dir[1]
                while qY >= 0 and qX >= 0 and qY < len(g) and qX < len(g[0]):
                    if g[qY][qX] == 'o':
                        g[qY][qX] = '.'
                    qY, qX = qY+dir[0], qX+dir[1]  
            return g

        def dfs(y, x, g):
            ## Find valid place to place queen, create recursion if found.
            while y < len(g):
                while x < len(g[0]):
                    if g[y][x] == 'o':
                        gCopy = deepcopy(g)
                        g[y][x] = 'Q'
                        g = QueensVision(y, x, g)
                        dfs(y, x+1, gCopy)
                    x += 1
                if 'Q' not in g[y]: return ## Early termination if no queen is in the row.
                y += 1; x = 0
            ## Only valid answers will make it this far.
            ## Convert them to the appropriate array of strings.
            for row in g: ans[-1].append("".join(row))
            ans.append([])

        dfs(0, 0, [['o']*n for x in range(n)])
        return ans[:-1]