import copy

def board_len(value): # builds the board 
    board = []
    for x in range(value):  # empty loop to loop n times
        row = []
        for v in range(value): # empty loop to loop n times
            row.append(0)
        board.append(row)
    return(board)

# for index, item in enumerate(board_len(5)):
#     print(str(index) + " " + str(item))


# check if piece is in danger

def is_in_danger(board, starting_row, current_column):
    # print("This is the board. Row: " + str(starting_row) + ", col: " + str(current_column))
    # for item in board:
    #     print(item)

    board_len = len(board)

    for index in range(board_len):
        if board[starting_row][index] == 0:
            pass
        else:
            return True

    for index in range(board_len):
        if board[index][current_column] == 0:
            pass
        else:
            return True

    index = 0
    while starting_row - index != -1 and current_column - index != -1: # up-left diagonal
        if board[starting_row - index][current_column - index] == 0:
            pass
        else:
            return True
        index += 1

    index = 0
    while starting_row + index != len(board) and current_column - index != -1: # down-left diagonal
        if board[starting_row + index][current_column - index] == 0:
            pass
        else:
            return True
        index += 1

    return False


def place_queen(board, current_column, finished_board):
    if current_column >= len(board):
        return finished_board
    for row_number in range(len(board)):
        
        if not is_in_danger(board, row_number, current_column):
            board[row_number][current_column] = 1
            # print("This is the current Row: ")
            # print(board[row_number])
            place_queen(board, current_column + 1, finished_board)
            if current_column == len(board) - 1: 
                # print("kkkkk")
                # print(board)
                copyboard = copy.copy(board)
                print("This is the copy: ")
                print(copyboard)
                finished_board.append(copy.deepcopy(board))
            board[row_number][current_column] = 0
        # else:
            # print("I'm in danger: ")
    # print("This is a finished board: ")
    # print(finished_board)
    return finished_board

def n_queens(board_size, current_column):
    board = board_len(board_size)
    solution = place_queen(board, current_column, [])
    for item in solution:
        for item2 in item:
            print(item2)
        print("--------------")
    print(len(solution))
    
    pass

n_queens(4, 0)