from pprint import pprint

def is_in_danger(board, row_index, column_index):
    #check the row for a rook
    for index, value in enumerate(board[row_index]):
        if value == 0 or index == column_index:
            pass
        else:
            return True

    # check a column for a rook BUT we do not need this because we are in control of what column we are putting the rook in
    # for index, row in enumerate(board):
    #     if row[column_index] == 0 or index == row_index:
    #         pass
    #     else:
    #         return True

    return False

def place_rook_in_column(board, column_number):
    if column_number >= 4:  # base case (ran out of board)
        return

    for row_number in range(4): # keep trying rows until we find one 
        board[row_number][column_number] = 1
        if is_in_danger(board, row_number, column_number):
            board[row_number][column_number] = 0   #removing it from the board
        else:
            place_rook_in_column(board, column_number + 1)



def four_rooks():
    board = [
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ]

    place_rook_in_column(board, 0)

    return board

if __name__ == "__main__":
    board = four_rooks()
    pprint(board, width=20)